Write a program to generate fibonacci numbers. It should work as follows:

```
How many fibonacci numbers to generate: 7

1, 2, 3, 5, 8, 13, 21
```
